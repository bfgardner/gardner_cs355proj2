var express = require('express');
var router = express.Router();
var doctor_dal = require('../model/doctor_dal');

// View All doctors
router.get('/all', function(req, res) {
    doctor_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('doctor/doctorViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query._number == null) {
        res.send('Ruh Roh something went wrong!');
    }
    else {
        doctor_dal.getById(req.query._number, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('doctor/doctorViewById', {'doctor': result[0][0], 'traits': result[1]});
            }
        });
    }
});

//return the enter a doctor form
router.get('/add/doctorChoose', function(req, res){
    // passing all the query parameters (req.query) to the add function
    doctor_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('doctor/doctorChoose', {'result': result});
        }
    });
});

router.get('/add', function(req, res){
    if (req.query._number == null){
        res.send(err);
    }
    else{
        res.render('doctor/doctorAdd', {'number': req.query._number})
    }
})

router.get('/addTrait', function(req, res){
    if (req.query._number == null){
        res.send('We have a problem');
    }
    else{
        res.render('doctor/doctorAddTrait', {'result': req.query._number})
    }
})

router.post('/insertTrait' , function(req,res){
    if (req.body.doctor_number == null){
        res.send('Ruh roh');
    }
    else if (req.body.personality == null){
        res.send('Must provide a personality trait');
    }
    else {
        doctor_dal.insertTrait(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                doctor_dal.getById(req.body.doctor_number, function(err, result){
                    if (err){
                        res.send(err);
                    }
                    else{
                        res.render('doctor/doctorViewById', {'doctor' : result[0][0], 'traits' : result[1], was_successful:true})
                    }
                })
            }
        })
    }
})

router.post('/insert', function(req, res){
    if (req.body.actor == null){
        res.send("All fields must be filled");
    }
    else if (req.body.hairstyle == null){
        res.send("All fields must be filled");
    }
    else if (req.body.hair_color == null){
        res.send("All fields must be filled");
    }
    else if (req.body.eye_color == null){
        res.send("All fields must be filled");
    }
    else if (req.body.apparent_age == null){
        res.send("All fields must be filled");
    }
    else if (req.body.approx_age == null){
        res.send("All fields must be filled");
    }
    else{
        doctor_dal.insert(req.body, function (err, result){
            if (err){
                res.send(err);
            }
            else{
                doctor_dal.getAll(function(err, result){
                    if (err){
                        res.send(err);
                    }
                    else{
                        res.render('doctor/doctorViewAll', {'result': result, was_successful:true});
                    }
                });
            }
        })
    }
})

module.exports = router;