var express = require('express');
var router = express.Router();
var companion_dal = require('../model/companion_dal');
var doctor_dal = require('../model/doctor_dal');

// View All companions
router.get('/all', function(req, res) {
    companion_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('companion/companionViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query._name == null) {
        res.send('Ruh Roh something went wrong!');
    }
    else {
        companion_dal.getById(req.query._name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('companion/companionViewById', {'companion': result[0][0], 'relationship': result[1]});
            }
        });
    }
});

//return the enter a doctor form
router.get('/add/companionChoose', function(req, res){
    // passing all the query parameters (req.query) to the add function
    companion_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('companion/companionChoose', {'result': result});
        }
    });
});

router.get('/add', function(req, res){
    if (req.query._name == null){
        res.send(err);
    }
    else{
        doctor_dal.getAll(function(err, result){
            if (err){
                res.send(err)
            }
            else {
                res.render('companion/companionAdd', {'name': req.query._name, 'doctors': result})
            }
        })

    }
})


router.post('/insert', function(req, res){

    if (req.body.hairstyle == null){
        res.send("All fields must be filled");
    }
    else if (req.body.hair_color == null){
        res.send("All fields must be filled");
    }
    else if (req.body.eye_color == null){
        res.send("All fields must be filled");
    }
    else if (req.body.approx_age == null){
        res.send("All fields must be filled");
    }
    else if (req.body.doctor == null){
        res.send("All fields must be filled")
    }
    else if (req.body.relationship == null){
        res.send("All fields must be filled")
    }
    else{
        companion_dal.insert(req.body, function (err, result){
            if (err){
                res.send(err);
            }
            else{
                companion_dal.getAll(function(err, result){
                    if (err){
                        res.send(err);
                    }
                    else{
                        res.render('companion/companionViewAll', {'result': result, was_successful:true});
                    }
                });
            }
        })
    }
})

module.exports = router;