var express = require('express');
var router = express.Router();
var otherCharacter_dal = require('../model/otherCharacter_dal');
var doctor_dal = require('../model/doctor_dal');
var species_dal = require('../model/species_dal');

// View All character
router.get('/all', function(req, res) {
    otherCharacter_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('otherCharacter/otherCharacterViewAll', { 'result':result[0], 'home':result[1] });
        }
    });

});

router.get('/', function(req, res){
    if(req.query._name == null) {
        res.send('Ruh Roh something went wrong!');
    }
    else {
        otherCharacter_dal.getById(req.query._name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('otherCharacter/otherCharacterViewById', {'otherCharacter': result[0][0], 'relationship': result[1]});
            }
        });
    }
});


router.get('/add/otherCharacterChoose', function(req, res){
    // passing all the query parameters (req.query) to the add function
    otherCharacter_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('otherCharacter/otherCharacterChoose', {'result': result});
        }
    });
});

router.get('/add', function(req, res){
    if (req.query._name == null){
        res.send(err);
    }
    else{
        doctor_dal.getAll(function(err, doctors){
            if (err){
                res.send(err)
            }
            else {
                species_dal.getAll(function(err, species){
                    if (err){
                        res.send(err)
                    }
                    else{
                        res.render('otherCharacter/otherCharacterAdd', {'name': req.query._name, 'doctors': doctors, 'species': species})
                    }
                })

            }
        })

    }
})


router.post('/insert', function(req, res){

    if (req.body.hairstyle == null){
        res.send("All fields must be filled");
    }
    else if (req.body.hair_color == null){
        res.send("All fields must be filled");
    }
    else if (req.body.eye_color == null){
        res.send("All fields must be filled");
    }
    else if (req.body.approx_age == null){
        res.send("All fields must be filled");
    }
    else if (req.body.doctor == null){
        res.send("All fields must be filled")
    }
    else if (req.body.relationship == null){
        res.send("All fields must be filled")
    }
    else if (req.body.species == null){
        res.send("All fields must be filled")
    }
    else{
        otherCharacter_dal.insert(req.body, function (err, result){
            if (err){
                res.send(err);
            }
            else{
                otherCharacter_dal.getAll(function(err, result){
                    if (err){
                        res.send(err);
                    }
                    else{
                        res.render('otherCharacter/otherCharacterViewAll', {'result': result[0], 'home': result[1], was_successful:true});
                    }
                });
            }
        })
    }
})

module.exports = router;