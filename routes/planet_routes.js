var express = require('express');
var router = express.Router();
var planet_dal = require('../model/planet_dal');

// View All doctors
router.get('/all', function(req, res) {
    planet_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('planet/planetViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.planet_name == null) {
        res.send('Ruh Roh something went wrong!');
    }
    else {

        planet_dal.getById(req.query.planet_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('planet/planetViewById', {'planet': result[0]});
            }
        });
    }
});

//return the enter a doctor form
router.get('/add/planetChoose', function(req, res){
    // passing all the query parameters (req.query) to the add function
    planet_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('planet/planetChoose', {'result': result});
        }
    });
});

router.get('/add', function(req, res){
    if (req.query.planet_name == null){
        res.send(err);
    }
    else{
        res.render('planet/planetAdd', {'planet': req.query.planet_name})
    }
})


router.post('/insert', function(req, res){
    if (req.body.description == null){
        res.send("All fields must be filled");
    }
    else if (req.body.atmosphere == null){
        res.send("All fields must be filled");
    }
    else if (req.body._language == null){
        res.send("All fields must be filled");
    }
    else{
        planet_dal.insert(req.body, function (err, result){
            if (err){
                res.send(err);
            }
            else{
                planet_dal.getAll(function(err, result){
                    if (err){
                        res.send(err);
                    }
                    else{
                        res.render('planet/planetViewAll', {'result': result, was_successful:true});
                    }
                });
            }
        })
    }
})

module.exports = router;