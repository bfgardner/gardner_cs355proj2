var express = require('express');
var router = express.Router();
var species_dal = require('../model/species_dal');
var planet_dal = require('../model/planet_dal');
// View All doctors
router.get('/all', function(req, res) {
    species_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('species/speciesViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.species_name == null) {
        res.send('Ruh Roh something went wrong!');
    }
    else {
        species_dal.getById(req.query.species_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('species/speciesViewById', {'species': result[0][0], 'traits': result[1]});
            }
        });
    }
});

//return the enter a species form
router.get('/add/speciesChoose', function(req, res){
    // passing all the query parameters (req.query) to the add function
    res.render('species/speciesChoose');
});

router.get('/add', function(req, res){
    if (req.query.species_name == null){
        res.send(err);
    }
    else{
        planet_dal.getAll(function(err, result){
            if (err){
                res.send(err)
            }
            else {
                res.render('species/speciesAdd', {'name': req.query.species_name, 'planets': result})
            }
        })

    }
})

router.get('/addTrait', function(req, res){
    if (req.query.species_name == null){
        res.send('We have a problem');
    }
    else{
        res.render('species/speciesAddTrait', {'result': req.query.species_name})
    }
})

router.post('/insertTrait' , function(req,res){
    if (req.body.species_name == null){
        res.send('Ruh roh');
    }
    else if (req.body.traits == null){
        res.send('Must provide a personality trait');
    }
    else {
        species_dal.insertTrait(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                species_dal.getById(req.body.species_name, function(err, result){
                    if (err){
                        res.send(err);
                    }
                    else{
                        res.render('species/speciesViewById', {'species' : result[0][0], 'traits' : result[1], was_successful:true})
                    }
                })
            }
        })
    }
})

router.post('/insert', function(req, res){
    if (req.body.description == null){
        res.send("All fields must be filled");
    }
    else if (req.body.num_appendages == null){
        res.send("All fields must be filled");
    }
    else if (req.body.hair == null){
        res.send("All fields must be filled");
    }
    else if (req.body.planet_name == null){
        res.send("All fields must be filled");
    }
    else if (req.body.skin_color == null){
        res.send("All fields must be filled");
    }
    else if (req.body.number_eyes == null){
        res.send("All fields must be filled");
    }
    else{
        species_dal.insert(req.body, function (err, result){
            if (err){
                res.send(err);
            }
            else{
                species_dal.getAll(function(err, result){
                    if (err){
                        res.send(err);
                    }
                    else{
                        res.render('species/speciesViewAll', {'result': result, was_successful:true});
                    }
                });
            }
        })
    }
})

module.exports = router;