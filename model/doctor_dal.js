var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM doctor';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(_number, callback) {
    var query = 'CALL doctor_getinfo(?);'
   // var query = 'SELECT * FROM doctor d WHERE d._number = ?;';
    var queryData = [_number];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'INSERT INTO doctor (_number, actor, hair_color, hairstyle, eye_color, apparent_age, approx_age) VALUES (?, ? ,?, ?, ?, ?, ?)';

    var queryData = [params._number, params.actor, params.hair_color, params.hairstyle, params.eye_color, params.apparent_age, params.approx_age];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insertTrait = function(params, callback){
  var query = 'INSERT INTO doctor_personality(doctor_number, personality) VALUES(?, ?)';

    var queryData = [params.doctor_number, params.personality];
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

