var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM planet';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(planet_name, callback) {
    var query = 'SELECT * FROM planet p WHERE p.planet_name = ?;'
    // var query = 'SELECT * FROM doctor d WHERE d._number = ?;';
    var queryData = [planet_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'INSERT INTO planet(planet_name, description, atmosphere, _language) VALUES (?,?,?,?)';
    var queryData = [params.planet_name, params.description, params.atmosphere, params._language];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

