var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM species';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(species_name, callback) {
    var query = 'CALL species_getinfo(?);'
    // var query = 'SELECT * FROM doctor d WHERE d._number = ?;';
    var queryData = [species_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'INSERT INTO species (species_name, description, number_appendages, hair, planet_name, skin_color, number_eyes) VALUES (?, ? ,?, ?, ?, ?, ?)';

    var queryData = [params.species_name, params.description, params.num_appendages, params.hair, params.planet_name, params.skin_color, params.number_eyes];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insertTrait = function(params, callback){
    var query = 'INSERT INTO species_traits(species_name, traits) VALUES(?, ?)';

    var queryData = [params.species_name, params.traits];
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};
