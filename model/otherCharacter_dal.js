var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    //var query = 'SELECT * FROM other_character';
    var query = 'CALL otherCharacterALl';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(_name, callback) {
    var query = 'CALL otherCharacter_getinfo(?);'
    // var query = 'SELECT * FROM doctor d WHERE d._number = ?;';
    var queryData = [_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO other_character (_name, hair_color, hairstyle, eye_color, approx_age, species) VALUES (?, ? ,?, ?, ?, ?)';

    var queryData = [params._name, params.hair_color, params.hairstyle, params.eye_color, params.approx_age, params.species];

    connection.query(query, queryData, function (err, result) {
        // callback(err, result);
    });

    query = 'INSERT INTO doctor_other_relationship (_character, doctor, relationship) VALUES (?, ?, ?)';
    queryData = [params._name, params.doctor, params.relationship];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};